<?php

use Illuminate\Support\Facades\Route;
use Ekoukltd\UserNotificationPrefs\Http\Controllers\ModalController;
use Ekoukltd\UserNotificationPrefs\Http\Controllers\NotifiablePreferencesController;
use Ekoukltd\UserNotificationPrefs\Http\Controllers\NotificationTypesController;
use Ekoukltd\UserNotificationPrefs\Http\Controllers\NotificationLogController;

$adminPrefix = config('user-notification-prefs.routes.admin.prefix');

Route::group(['middleware' => config('user-notification-prefs.routes.admin.middleware'),
             'prefix' => $adminPrefix], function() use ($adminPrefix){
    //Routes for admins to update Notification Types
    //List the notification types
    Route::get("/", [NotificationTypesController::class, 'index'])->name($adminPrefix.'.index');
    //Load Create a new one
    Route::get("/new", [NotificationTypesController::class, 'create'])->name($adminPrefix.'.create');
    //Store a new one
    Route::post("/", [NotificationTypesController::class, 'store'])->name($adminPrefix.'.store');
    //Show a record
    Route::get("/{notificationType}", [NotificationTypesController::class, 'show'])->name($adminPrefix.'.show');
    //Show the edit form
    Route::get("/{notificationType}/edit", [NotificationTypesController::class, 'edit'])->name($adminPrefix.'.edit');
    Route::post("/{notificationType}/emit", [NotificationTypesController::class, 'emit'])->name($adminPrefix.'.emit');
    //Update a record
    Route::match(['put','patch'],"/{notificationType}", [NotificationTypesController::class, 'update'])->name($adminPrefix.'.update');
	//Delete a record
    Route::delete('notification-types/{notificationType}', [NotificationTypesController::class, 'destroy'])->name($adminPrefix.'.destroy');
    
    //modals
	Route::get('confirm-delete/{id}', [ModalController::class, 'confirmDelete'])->name('unp.modal.confirm.delete');
	Route::get('confirm-emit/{notificationType}', [ModalController::class, 'confirmEmit'])->name('unp.modal.confirm.emit');

    
});

Route::group(['middleware' => config('user-notification-prefs.routes.user.middleware'),
              'prefix' => config('user-notification-prefs.routes.user.prefix')], function(){
	//Routes for users to update notification preferences
	//List notification preferences
	Route::get('/', [NotifiablePreferencesController::class, 'index'])->name(config('user-notification-prefs.routes.user.prefix').'.index');
	//Update notification preference
	Route::post('/', [NotifiablePreferencesController::class, 'update'])->name(config('user-notification-prefs.routes.user.prefix').'.update');
});


Route::group(['middleware' => config('user-notification-prefs.routes.logs.middleware'),
              'prefix' => config('user-notification-prefs.routes.logs.prefix')], function(){
    //Routes for users to update notification preferences
    //List notification preferences
    Route::get('/', [NotificationLogController::class, 'index'])->name(config('user-notification-prefs.routes.logs.prefix').'.index');
});