<?php

namespace Ekoukltd\UserNotificationPrefs\Http\Controllers;

use Ekoukltd\UserNotificationPrefs\Http\Requests\NotifiablePreferencesFormRequest;
use Ekoukltd\UserNotificationPrefs\Models\NotifiablePreferences;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotifiablePreferencesController extends Controller
{
    public function index()
    {
        if (!method_exists(Auth::user(), 'notificationPreferences')) {
            abort('500', 'Please add the HasNotificationPreferences Trait to this user model');
        }
        
        return view('vendor.ekoukltd.user-notification-prefs.notifiable-preferences.index')->with(
            [
                'notificationTypes' => Auth::user()
                                           ->availablePreferences()
                                           ->get(),
                'availableVias'     => array_diff(
                    config('user-notification-prefs.default_vias'),
                    config('user-notification-prefs.disallow_via_preferences')
                )
            ]
        );
    }
    
    public function update(NotifiablePreferencesFormRequest $request)
    {
        try {
            if ($notifiablePreference = NotifiablePreferences::find($request->id)) {
                $notifiablePreference->toggleVia($request->via);
            }
            else {
                //Create new Preference
                $notifiablePreference = NotifiablePreferences::build($request->notification_type_id);
                if (in_array($request->via, $notifiablePreference->via)) {
                    if (!$request->checked) {
                        $notifiablePreference->toggleVia($request->via);
                    }
                }
                else {
                    if ($request->checked) {
                        $notifiablePreference->toggleVia($request->via);
                    }
                }
            }
            
            return response()->json(
                [
                    'success' => true,
                    'message' => 'Preference updated'
                ]
            );
        } catch (\Exception $exception) {
            return response()->json(
                [
                    'error' => true,
                    'message' => 'There was an error updating your preference'
                ]
            );
        }
    }
}
