<?php

namespace Ekoukltd\UserNotificationPrefs\Http\Controllers;

use Ekoukltd\UserNotificationPrefs\Helpers\Helper;
use Ekoukltd\UserNotificationPrefs\Http\Requests\NotificationTypeFormRequest;
use Ekoukltd\UserNotificationPrefs\Datatables\NotificationTypesDatatables;
use Ekoukltd\UserNotificationPrefs\Models\NotificationType;
use Illuminate\Support\Facades\Auth;

class NotificationTypesController extends Controller
{
    public function index(NotificationTypesDatatables $dataTable)
    {
        if (request()->ajax()) {
            return $dataTable->ajax();
        }
        
        return view('vendor.ekoukltd.user-notification-prefs.notification-types.index')->with(
            ['dataTable' => $dataTable->html()]
        );
    }
    
    public function create()
    {
        $model = new NotificationType();
        return view('vendor.ekoukltd.user-notification-prefs.notification-types.create', compact("model"));
    }
    
    public function store(NotificationTypeFormRequest $request)
    {
		
        $notificationType = NotificationType::create($request->getData());
        try {
            $notificationType->save();
            return redirect(
                route(config('user-notification-prefs.routes.admin.prefix').'.show', compact('notificationType'))
            )->with(['success' => 'New notification type has been created.']);
        } catch (\Exception $exception) {
            return redirect(
                route(config('user-notification-prefs.routes.admin.prefix').'.create')
            )->with(['error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }
    
    public function show(NotificationType $notificationType)
    {
        return view('vendor.ekoukltd.user-notification-prefs.notification-types.show', compact('notificationType'));
    }
    
    public function edit(NotificationType $notificationType)
    {
        return view('vendor.ekoukltd.user-notification-prefs.notification-types.edit')->with(['model' => $notificationType]);
    }
    
    public function update(NotificationTypeFormRequest $request,
                           NotificationType            $notificationType)
    {
        $data = $request->getData();
        try {
            $notificationType->update($data);
            return redirect(route(config('user-notification-prefs.routes.admin.prefix').'.show',['notificationType'=>$notificationType]))->with(
                ['success' => $notificationType->name.'has been updated.']
            );
        } catch (\Exception $exception) {
            return redirect(
                route(config('user-notification-prefs.routes.admin.prefix').'.edit', ['notificationType' => $notificationType])
            )->with(['error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }
    
    public function emit( NotificationType $notificationType)
    {
        
        $class= $notificationType->className;
        
        try {
            Auth::user()->notify(new $class());
        } catch (\Exception $ex){
            return back()->with(['error'=>$ex->getMessage()]);
        }
    
        return back()->with(['success'=>$notificationType->nameHeadline.' notification has been sent']);
    }
    
    public function destroy(NotificationType $notificationType)
    {
        $notificationTypeName = $notificationType->name;
        try {
            $notificationType->delete();
            
            return back()
                ->with(['success' => $notificationTypeName.' was successfully deleted.']);
        } catch (\Exception $exception) {
            return back()
                ->withInput()
                ->withErrors(['error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }
}
