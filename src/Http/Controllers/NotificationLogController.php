<?php

namespace Ekoukltd\UserNotificationPrefs\Http\Controllers;

use Ekoukltd\UserNotificationPrefs\Datatables\NotificationLogDatatables;
use Illuminate\Support\Facades\Auth;

class NotificationLogController extends Controller
{
    public function index(NotificationLogDatatables $dataTable)
    {
        if (request()->ajax()) {
            return $dataTable->ajax();
        }
        
        return view('vendor.ekoukltd.user-notification-prefs.notification-logs.index')->with(
            ['dataTable' => $dataTable->html()]
        );
    }
}
