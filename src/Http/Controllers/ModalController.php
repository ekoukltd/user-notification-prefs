<?php

namespace Ekoukltd\UserNotificationPrefs\Http\Controllers;

use Ekoukltd\UserNotificationPrefs\Models\NotificationType;
use Illuminate\Http\Request;

class ModalController extends Controller
{
	public function confirmDelete(Request $request, $id)
	{
		$model = NotificationType::where('id', $id)->first();
		return view('vendor.ekoukltd.user-notification-prefs.components.modals.confirm_delete')->with(['model' => $model]);
	}
    
    public function confirmEmit(NotificationType $notificationType)
    {
        return view('vendor.ekoukltd.user-notification-prefs.components.modals.confirm_emit')->with(['model' => $notificationType]);
    }
}
