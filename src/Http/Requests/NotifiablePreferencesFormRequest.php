<?php

namespace Ekoukltd\UserNotificationPrefs\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class NotifiablePreferencesFormRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rules = [
			'checked' 			   => 'boolean',
			'via' 				   => ['string', Rule::in(config('user-notification-prefs.default_vias'))],
			'id' 				   => 'integer',
			'notification_type_id' => 'integer'
		];

		return $rules;
	}
}