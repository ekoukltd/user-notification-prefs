<?php

namespace Ekoukltd\UserNotificationPrefs\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class NotificationTypeFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'            => 'required|string|max:191',
            'recipient_type'  => [
                'array',
                Rule::in(config('user-notification-prefs.models'))
            ],
            'default_via'     => [
                'array',
                Rule::in(config('user-notification-prefs.default_vias'))
            ],
            'properties'      => 'array|nullable',
            'email_template'  => 'string|nullable',
            'email_injector'  => 'array|nullable',
            'default_toarray' => 'array',
            'sms_content'     => 'string|nullable',
            'readonly'       => 'boolean',
            'hidden'          => 'boolean'
        ];
        
        return $rules;
    }
    
    /**
     * Get the request's data from the request.
     *
     *
     * @return array
     */
    public function getData()
    {
        return $this->only([
                               'name',
                               'recipient_type',
                               'default_via',
                               'properties',
                               'email_template',
                               'email_injector',
                               'default_toarray',
                               'sms_content',
                               'readonly',
                               'hidden'
                           ]);
    }
    
    protected function prepareForValidation(): void
    {
        //This is coming in as a string representation of an array
        //remove extra whitspace, newlines and tabs
        $default_toarray = preg_replace('/\s+/S', " ", $this->default_toarray);
        
        //Remove the first [
        $pos = strpos($default_toarray, "[");
        if ($pos !== false) {
            $default_toarray = substr_replace($default_toarray, "", $pos, 1);
        }
        //Remove the last ]
        $pos = strrpos($default_toarray, "]");
        if ($pos !== false) {
            $default_toarray = substr_replace($default_toarray, "", $pos, 1);
        }
        
        $allowableEmptyArrays = [
            'properties',
            'default_via',
            'email_injector'
        ];
        
        foreach ($allowableEmptyArrays as $property) {
            if (!isset($this->$property)) {
                $this->$property = [];
                $this->merge([$property => []]);
            }
        }
		
		$props = [];
		
		if(!in_array('mail',$this->default_via)){
			$this->merge(['email_template' => null]);
		}
	
	    if(!in_array('sms',$this->default_via)){
		    $this->merge(['sms_content' => null]);
	    }
		
		//Remove $ from properties if the user has added one
	    foreach($this->properties as $key=> $property){
			$props[]=ltrim($property, '$');
	    }
		
		
        $this->merge(['properties' =>$props ]);
        
        $this->merge([
                         'name'            => Str::studly($this->name),
                         //turn the string into an array
                         'default_toarray' => explode(",", $default_toarray),
                         'readonly'       => (bool) $this->has('readonly'),
                         'hidden'           => (bool) $this->has('hidden')
                     ]);
    }
}