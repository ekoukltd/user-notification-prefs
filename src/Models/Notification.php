<?php

namespace Ekoukltd\UserNotificationPrefs\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Stevebauman\Location\Facades\Location;

/**
 * @property string $id
 * @property string $type
 * @property string $notifiable_type
 * @property integer $notifiable_id
 * @property string $data
 * @property string $read_at
 * @property string $created_at
 * @property string $updated_at
 */
class Notification extends DatabaseNotification
{
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';
    protected $casts = [
        'data' => 'array',
    ];
    
    /**
     * @return mixed
     */
    public static function getAllUserTypes() {
        $defaults = config('user-notification-prefs.models');
        $models   = [];
        foreach ($defaults as $model) {
            $models[] = [
                'id' => $model,
                'name' => class_basename($model),
                'relation' => strtolower(class_basename($model)),
                'table' => with(new $model)->getTable(),
                'searchable' => with(new $model)->searchableNameAttributes()
            ];
        }
        return json_decode(json_encode($models));
    }
    
    /**
     * Sentence Formatted Name
     *
     * @return string
     */
    public function getNameHeadlineAttribute() {
        return Str::headline(str_replace('Notification', '', class_basename($this->type)));
    }
    
    /**
     * @return mixed|null
     */
    public function getAgentDataAttribute() {
        if(array_key_exists('agent', $this->data)) {
            return json_decode(json_encode($this->data[ 'agent' ]));
        }
        return null;
    }
    
    /**
     * Get IP Address
     *
     * @return string
     */
    public function getIpAttribute() {
        return $this->agentData ? $this->agentData->ip : '';
    }
    
    /**
     * Lookup city from IP address and update the notification log
     * This method means lookup is made once for each record but only when the notification is viewed.
     *
     * So we can not search for cities if they haven't been geocoded yet.
     * Better method would be to lookup every request when the event is generated but
     * Due to having issues before with IP lookups leaving here for the moment
     *
     * @return string|void
     */
    public function getCityAttribute() {
        if($data = $this->agentData) {
            if(!property_exists($data, 'city')) {
                if($position = Location::get($data->ip)) {
                    $city = trim($position->cityName." ".$position->zipCode);
                    $this->update(['data->agent->city' => $city]);
                    return $city;
                }
            }
            else {
                return $data->city;
            }
        }
    }
    
    /**
     * @return string|void
     */
    public function getAgentDataStringAttribute() {
        if($data = $this->agentData) {
            $res = '<p class="mb-0"><b>IP</b>: '.property_exists($data, 'ip') ? $data->ip : ''."</p>";
            $res .= '<p class="mb-0"><b>Browser</b>: '.(property_exists($data, 'browser') ? $data->browser : '')." "
                    .(property_exists($data, 'browserVersion') ? $data->browserVersion : '')."</p>";
            $res .= '<p class="mb-0"><b>Device</b>: '.(property_exists($data, 'device') ? $data->device : '')."</p>";
            $res .= '<p class="mb-0"><b>Plaform</b>: '.(property_exists($data, 'platform') ? $data->platform : '')." "
                    .$data->platformVersion."</p>";
            return $res;
        }
    }
    
    public function getDeviceIconAttribute() {
        if($data = $this->agentData) {
            return ($data->isDesktop ? 'laptop' : ($data->isMobile ? 'mobile-alt' : 'tablet-alt'));
        }
        return 'question-circle';
    }
}
