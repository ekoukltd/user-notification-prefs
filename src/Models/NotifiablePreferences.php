<?php
namespace Ekoukltd\UserNotificationPrefs\Models;

use Illuminate\Database\Eloquent\Relations\MorphPivot;
use Illuminate\Support\Facades\Auth;

/**
 * @property integer $id
 * @property integer $notification_type_id
 * @property integer $notifiable_id
 * @property string $notifiable_type
 * @property string $via
 */


class NotifiablePreferences  extends MorphPivot
{
    public $incrementing = true;
    
    protected $table = 'notifiable_preferences';

    protected $casts = ['via' => 'array'];
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notificationType()
    {
        return $this->belongsTo(NotificationType::class,'notification_type_id','id',);
    }
    
    /**
     * @param $via
     * @return bool
     */
    public function toggleVia($via){
        if(in_array($via,$this->via)){
            //Remove item from array
            $this->via = array_values(array_diff($this->via, [$via]));
        } else {
            //Add item to array if toggle checked
            $this->via = array_merge($this->via,[$via]);
        }
        return $this->save();
    }
    
    public static function build($notificationType)
    {
        //Create new Preference
        $notifiablePreference = new self();
        $notifiablePreference->via = NotificationType::findOrFail($notificationType)->default_via;
        $notifiablePreference->notification_type_id = $notificationType;
        $notifiablePreference->notifiable_id = Auth::id();
        $notifiablePreference->notifiable_type = get_class(Auth::user());
        $notifiablePreference->save();
        return $notifiablePreference;
    }
}