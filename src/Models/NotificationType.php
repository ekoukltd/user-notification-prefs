<?php
namespace Ekoukltd\UserNotificationPrefs\Models;

use App\Models\User;
use Ekoukltd\UserNotificationPrefs\Helpers\Helper;
use Ekoukltd\UserNotificationPrefs\Helpers\Highlight;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Symfony\Component\Console\Output\BufferedOutput;



/**
 * @property integer $id
 * @property string $name
 * @property mixed $recipient_type
 * @property mixed $default_via
 * @property mixed $properties
 * @property string $email_template
 * @property string $sms_content
 * @property mixed $email_injector
 * @property mixed $default_toarray
 * @property boolean $hidden
 * @property boolean $read_only
 * @property string $created_at
 * @property string $updated_at
 */
class NotificationType extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['name', 'recipient_type', 'default_via', 'properties', 'email_template', 'read_only','hidden','email_injector', 'sms_content','default_toarray', 'created_at', 'updated_at'];
    
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'recipient_type' => 'array',
        'default_via' => 'array',
        'properties' => 'array',
        'email_injector' => 'array',
        'default_toarray' => 'array',
        'read_only' =>'boolean',
        'hidden' =>'boolean',
        'created_at' =>'datetime:Y-m-d H:i:s',
        'updated_at' =>'datetime:Y-m-d H:i:s'
        
    ];
    
    /**
     * Write the Notification Class Model after creating or updating a NotificationType
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        self::created(function($model){
            $model->buildNotificationClass();
        });

        self::updated(function($model){
            $model->buildNotificationClass();
        });
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }
    
    private function buildNotificationClass()
    {
        $output = new BufferedOutput();
        Artisan::call('build:notification',[
            'name'=>$this->name,
            '--force' =>true,
            '--v'=>$this->default_via,
            '--p'=>$this->properties,
            '--m'=>$this->email_template,
            '--mi'=>$this->email_injector,
            '--a'=>$this->default_toarray,
            '--s'=>$this->sms_content,
        ],$output);
        $res = $output->fetch();
        if(substr( $res, 0, strlen("Notification created") ) === "Notification created"){
            Log::info($this->name. ' created.');
            return true;
        }
        Log::error($output->fetch());
        return false;
    }
    
    /**
     * Returns the full path to this generated class
     * @return string
     */
    public function getFileNameAttribute() :string
    {
        return app_path('Notifications/').$this->name.'.php';
    }
    
    /**
     * Returns the contents of this generated class
     * @return string
     */
    public function getClassContentsAttribute() :string
    {
        return file_get_contents($this->fileName);
    }
    
    /**
     * @return string
     */
    public function getPropertiesStringAttribute()
    {
        if(is_array($this->properties) && count($this->properties)){
           return "$".implode( ", $",$this->properties)." ";
           }
        return "";
    }
    
    /**
     * Shows code to implement
     * @return string
     */
    public function getUsageExampleAttribute()
    {
        $class= $this->className;
        $response = "<p>use $class;</p>";
        foreach($this->recipient_type as $recipient)
        {
            $response .= "<p class='mb-0'>".Highlight::render("\$".Str::camel(class_basename($recipient))."->notify(new ".$this->name."(".$this->propertiesString."));")."</p>";
        }
        
        return $response;
    }
    
    public function getClassContentsFormattedAttribute()
    {
        Highlight::showLineNumber(true);
        return Highlight::render($this->classContents, false, true);
    }
    
    public function getClassNameAttribute()
    {
        $nameSpace = app()->getNamespace()."Notifications";
        return $nameSpace."\\".$this->name;
    }
    
    public function getNameHeadlineAttribute()
    {
        return Str::headline(str_replace('Notification','',$this->name));
    }
    
    public static function getNotificationTypeSelect()
    {
        
        $collection =  self::all()->pluck('name')->map(function ($item) {
            return (object) [
                'id' => $item,
                'name' => Str::headline(str_replace('Notification','',$item))
            ];
        });
        $all = (object) [
            'id' => "",
            'name' => "All"
        ];
        $collection->prepend($all);
        return $collection;
    
    }
}
