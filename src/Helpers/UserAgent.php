<?php

namespace Ekoukltd\UserNotificationPrefs\Helpers;


use Jenssegers\Agent\Agent;

class UserAgent

{

    public static function getAgentProperties()
    {
        $agent = new Agent();
        $device = $agent->device();
        $platform = $agent->platform();
        $browser = $agent->browser();
        $userData = [
            'browser' => $browser,
            'browserVersion' => $agent->version($browser),
            'device'  => $device,
            'platform'=>$platform,
            'platformVersion' =>$agent->version($platform),
            'isMobile' =>$agent->isMobile(),
            'isTablet' => $agent->isTablet(),
            'isDesktop'=>$agent->isDesktop(),
            'ip' => self::getIp()
        ];
        return $userData;
      
    }
    
    public static function getIp(){
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
        return request()->ip(); // it will return server ip when no client ip found
    }
}