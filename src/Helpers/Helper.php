<?php

namespace Ekoukltd\UserNotificationPrefs\Helpers;

use Ekoukltd\UserNotificationPrefs\Models\NotificationType;
use Illuminate\Container\Container;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class Helper

{
    /**
     * Find all Models in use in the system
     *
     * @return Collection
     */
    public static function getModels(): Collection
    {
        $models = collect(File::allFiles(app_path()))
            ->map(function ($item) {
                $path  = $item->getRelativePathName();
                $class = sprintf(
                    '\%s%s',
                    Container::getInstance()
                             ->getNamespace(),
                    strtr(substr($path, 0, strrpos($path, '.')), '/', '\\')
                );
                
                return $class;
            })
            ->filter(function ($class) {
                $valid = false;
                
                if (class_exists($class)) {
                    $reflection = new \ReflectionClass($class);
                    $valid      = $reflection->isSubclassOf(Model::class) &&
                        !$reflection->isAbstract();
                }
                
                return $valid;
            });
        
        return $models->values();
    }
    
    public static function getModelsAsProperties()
    {
        //Get all model classes
        $models = Helper::getModels();
        
        //Strip out namespace -> we only need the basename in camelcase
        $models = $models->map(function ($item) {
            $path = explode('\\', $item);
            return Str::camel(array_pop($path));
        });
        
        $propertiesArray = $models->values()->toArray();
        
        //Get any custom values already stored in the database
        $properties = NotificationType::groupBy('properties')->pluck('properties')->toarray();
        
        //each property is an array -> split out to single array of unique values
        $dbProperties = [];
        foreach($properties as $propArray) {
            foreach ($propArray as $value) {
                $dbProperties[] = $value;
            }
        }
        $propertiesCollection = collect(array_merge_recursive(array_unique($dbProperties),$propertiesArray));
        $propertiesCollection->sort();
    
        return $propertiesCollection->map(function ($item) {
            return (object)['id'=>$item,'name'=>"$".$item];
        });
    }
    
    /**
     * Find all Mail templates in use in the system
     *
     * @return Collection
     */
    public static function getMails(): Collection
    {
        $models = collect(File::allFiles(app_path('Mail')))
            ->map(function ($item) {
                $path  = $item->getRelativePathName();
                $class = sprintf(
                    '\%s%s',
                    Container::getInstance()
                             ->getNamespace()."Mail\\",
                    strtr(substr($path, 0, strrpos($path, '.')), '/', '\\')
                );
                
                return $class;
            })
            ->filter(function ($class) {
                $valid = false;
                
                if (class_exists($class)) {
                    $reflection = new \ReflectionClass($class);
                    $valid      =  $reflection->isSubclassOf(Mailable::class) &&
                        !$reflection->isAbstract();
                }
                
                return $valid;
            });
        
        return $models->values();
    }
    
    public static function getMailSelect()
    {
        $mails = self::getMails();
        $models =  $mails->map(function ($item) {
            $path = explode('\\', $item);
            return (object)['id'=>$item,'name'=>Str::headline(array_pop($path))];
        });
        
        return $models;
    }
    
    
    public static function getUserModelSelect()
    {
        //@SAM
        //Here's a useful trick
        //Convert array to collection
        $users = collect(config('user-notification-prefs.models'));
        
        //Then use collection map to create an array of objects
        return $users->map(function ($item) {
            return (object)['id'=>$item,'name'=>Str::title($item)];
        });



//        $users = config('user-notification-prefs.models');
//        $models = [];
//        foreach($users as $user) {
//            $temp = (object) [];
//            $temp->id = $user;
//            $temp->name = $user;
//            $models[] = $temp;
//        }
//        return $models;
    }
    
    public static function getDefaultViaSelect()
    {
        $vias = collect(config('user-notification-prefs.default_vias'));
        return $vias->map(function ($item) {
            return (object)['id'=>$item,'name'=>Str::title($item)];
        });
    }
    

    
    
    public static function mapViaToBadge($via){
        
        return '<span class="badge rounded-pill me-1 '.self::viaMappings($via,'bg').'">'.self::mapViaToIcon($via).' '.self::viaMappings($via,'text').'</span>';
    }
    
    
    public static function mapViaToIcon($via)
    {
        return self::getIcon(self::viaMappings($via,'icon'));
    }
    
    public static function viaMappings($via,$prop)
    {
        $vias = [
            'database' => ['bg'=>'bg-black-50','icon'=>'database','text'=>'db'],
            'mail'     => ['bg'=>'bg-warning','icon'=>'envelope','text'=>'email'],
            'sms'    => ['bg'=>'bg-danger','icon'=>'sms','text'=>'sms'],
            'push'     => ['bg'=>'bg-success','icon'=>'bell','text'=>'push']
        ];
        return array_key_exists($via,$vias) && array_key_exists($prop,$vias[$via])?$vias[$via][$prop]:null;
    }
    
    
    public static function getIcon($fa)
    {
        return '<i class="fa fa-'.$fa.'"></i>';
    }
}
