<?php

namespace Ekoukltd\UserNotificationPrefs\Console;

use Ekoukltd\UserNotificationPrefs\Models\NotificationType;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;

class BuildNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'build:notifications
        {--f|force : overwrite existing if exists}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create all notification classes from NotificationTypes models';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        
        $notifications = NotificationType::all();
        
        foreach($notifications as $notification)
        {
            try {
                $this->createClass($notification);
            } catch (\Exception $e){
                Log::error($e->getMessage());
            }
        }
        return 0;
    
    }
    
    /**
     * Create a basic database only notification class
     * @param $className
     * @return int
     */
    private function createClass($notification)
    {
        $options = $this->options();
        $output = new BufferedOutput();
        Artisan::call('build:notification',[
            'name'=>$notification->name,
            '--force' => $options['force'],
            '--v'=>$notification->default_via,
            '--p'=>$notification->properties,
            '--m'=>$notification->email_template,
            '--mi'=>$notification->email_injector,
            '--a'=>$notification->default_toarray,
        ],$output);
        
        $res = $output->fetch();
        
        if(substr( $res, 0, strlen("Notification created") ) === "Notification created"){
            $this->info($notification->name. ' created.');
            return true;
        }
       
    }
    protected function getOptions()
    {
        return [
            ['force', 'f', InputOption::VALUE_NONE, 'Create the class even if the notification already exists'],
        ];
    }
}
