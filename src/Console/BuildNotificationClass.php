<?php

namespace Ekoukltd\UserNotificationPrefs\Console;

use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;

class BuildNotificationClass  extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'build:notification';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new notification interface with parameters';
    
    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Notification';
    
    protected $signature = 'build:notification
        {name}
        {--f|force : overwrite existing if exists}
        {--p=* : properties of the notification}
        {--v=* : notification vias eg mail, database, slack}
        {--m= : mail class to use if sending mail.  Typically \App\Mail\XxxxxMail }
        {--mi=* : mail injectors to use will prepend $this->}
        {--a=* : To Array Options}
        {--s : SMS Content}
        
        ';
    
    /**
     * Replace the class name for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $options = $this->options();
        
        $stub = parent::replaceClass($stub, $name);
        
        $stub = str_replace('DummyContract', $this->argument('name'), $stub);
        
        $properties = "";
        $injectors = "";
        $setProperties = "";
        $via = 'false';
        $mailInjectors = "";
        $toArray="";
        $mailCall="";
        $smsContent ="";
		$injectorsStringReplacement = "{{ injectors }},";
		
		
        foreach($options['p'] as $prop)
        {
            $properties .="\tpublic \$".$prop.";\r\n";
            $injectors .="\$".$prop.",";
            $setProperties .="\t\t\$this->".$prop." = \$".$prop.";\r\n";
	        $injectorsStringReplacement = "{{ injectors }}";
        }
		
        
        if(count($options['v']))
        {
            $via = "'".implode("', '",$options['v'])."'";
        }
    
        foreach($options['mi'] as $mi)
        {
            $mailInjectors .=",\$this->".$mi;
        }
    
        if($options['m'] && strlen($options['m']))
        {
            $mail = $options['m'];
            $mailCall = "return (new {{ mail }}(\$notifiable {{ mailInjectors }} ));";
            $mailCall = str_replace('{{ mailInjectors }}', $mailInjectors, $mailCall);
            $mailCall = str_replace('{{ mail }}', $mail, $mailCall);
        }
    
        if(count($options['a']))
        {
            $toArray = implode(",",$options['a']);
        }
	
	    $toSms  = in_array('sms',$options['v'])?"return (new NexmoMessage)
		           ->content( {{ smsContent }})
            ->clientReference((string) \$notifiable->id);":'';
	
    
        $smsContent = $options['s'] && in_array('sms',$options['v'])?"\"".$options['s']."\"":"";
        
        $stub = str_replace('{{ properties }}', $properties, $stub);
        $stub = str_replace($injectorsStringReplacement, rtrim($injectors, ","), $stub);
        $stub = str_replace('{{ setProperties }}', $setProperties, $stub);
        $stub = str_replace('{{ via }}', "[ $via ]", $stub);
        $stub = str_replace('{{ toArray }}', "[ $toArray ]", $stub);
        $stub = str_replace('{{ toMail }}', $mailCall, $stub);
	    $stub = str_replace('{{ toSms }}', $toSms, $stub);
        $stub = str_replace('{{ smsContent }}', $smsContent, $stub);
       
        return $stub;
        
        
    }
    
    /**
     * Get the stub file for the generator
     * Try to get users custom file first
     *
     * @return string
     */
    protected function getStub()
    {
	    return file_exists(base_path(config('user-notification-prefs.stub')))
		    ?base_path(config('user-notification-prefs.stub'))
		    :__DIR__ . '/../Stubs/notification.stub';
    }
    
    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Notifications';
    }
    
    
    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the contract.'],
        ];
    }
    
    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['force', 'f', InputOption::VALUE_NONE, 'Create the class even if the notification already exists'],
            ['properties', 'p', InputOption::VALUE_IS_ARRAY, 'Properties to add for injection'],
            ['via', 'v', InputOption::VALUE_IS_ARRAY, 'Notification Vias eg database, mail, slack'],
            ['mail', 'm', InputOption::VALUE_OPTIONAL, 'MailClass if sending email'],
            
            
        ];
    }
}
