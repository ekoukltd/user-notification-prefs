<?php

namespace Ekoukltd\UserNotificationPrefs;


use Ekoukltd\UserNotificationPrefs\Console\BuildNotificationClass;
use Ekoukltd\UserNotificationPrefs\Console\BuildNotifications;
use Ekoukltd\UserNotificationPrefs\Models\Notification;
use Ekoukltd\UserNotificationPrefs\View\Alert;
use Ekoukltd\UserNotificationPrefs\View\ModalContent;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class UserNotificationPrefsServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'ekoukltd');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'ekoukltd');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/../routes/admin.php');

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
        $this->commands([
                            BuildNotificationClass::class,
                            BuildNotifications::class
                        ]);
		

    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__.'/../config/user-notification-prefs.php', 'user-notification-prefs');

        // Register the service the package provides.
        $this->app->singleton('user-notification-prefs', function ($app) {
            return new UserNotificationPrefs;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['user-notification-prefs'];
    }

    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole(): void
    {
        // Publishing the configuration file.
        $this->publishes([
                             __DIR__.'/../config/user-notification-prefs.php' => config_path('user-notification-prefs.php'),
        ], 'user-notification-prefs.config');

        // Publishing the views.
        $this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/ekoukltd/user-notification-prefs'),
        ], 'user-notification-prefs.views');
		
        // Publishing assets.
        $this->publishes([__DIR__.'/../resources/assets' => base_path('resources/assets/vendor/ekoukltd/user-notification-prefs'),
                         ], 'user-notification-prefs.views');
	
	    // Publishing the translation files.
        $this->publishes([__DIR__.'/../resources/lang' => base_path('resources/lang/vendor/ekoukltd/user-notification-prefs'),
                         ], 'user-notification-prefs.views');
	
	    // Publishing the stub file.
	    $this->publishes([__DIR__.'/Stubs' => base_path('app/Stubs'),
	                     ], 'user-notification-prefs.stubs');
		
        // Registering package commands.
        $this->commands([
            BuildNotificationClass::class,
            BuildNotifications::class
                        ]);
    }
}
