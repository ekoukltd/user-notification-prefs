<?php

namespace Ekoukltd\UserNotificationPrefs\Facades;

use Illuminate\Support\Facades\Facade;

class UserNotificationPrefs extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'user-notification-prefs';
    }
}
