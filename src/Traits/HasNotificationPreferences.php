<?php
namespace Ekoukltd\UserNotificationPrefs\Traits;

use Ekoukltd\UserNotificationPrefs\Models\NotificationType;
use Ekoukltd\UserNotificationPrefs\Models\NotifiablePreferences;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;

trait HasNotificationPreferences

{
	/**
	 * @return string[]
	 */
	public function searchableNameAttributes()
	{
		return ['name'];
	}
	
    /**
     * Gets all the defined preferences for this user
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function notificationPreferences()
    {
        return $this->morphMany(NotifiablePreferences::class, 'notifiable');
    }
    
    /**
     * @param  Builder  $query
     * @return mixed
     */
    public function scopeAvailablePreferences(Builder $query)
    {
        
        return NotificationType::selectRaw('notification_types.*, COALESCE(notifiable_preferences.id, false) as preference_id, COALESCE(notifiable_preferences.via, notification_types.default_via) as preference_via')
		->leftJoin('notifiable_preferences', function($join)
	   		{
				$join->on('notification_types.id','=','notifiable_preferences.notification_type_id')
					->where('notifiable_preferences.notifiable_id','=',$this->id)
					->where('notifiable_preferences.notifiable_type','=',[get_class($this)]);
			})->whereJsonContains('recipient_type', [get_class($this)]);

    }
	
    
}