<?php
namespace Ekoukltd\UserNotificationPrefs\Traits;

use Ekoukltd\UserNotificationPrefs\Models\NotificationType;
use Ekoukltd\UserNotificationPrefs\Models\NotifiablePreferences;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Notifications\AnonymousNotifiable;
use Illuminate\Support\Facades\Auth;

trait HasUserPreferences

{
    
    public function userVias($notifiable)
    {
	
	    if ($notifiable instanceof AnonymousNotifiable) {
		    return array_keys($notifiable->routes);
	    }
		
    	$notificationType = NotificationType::where(['name' => class_basename($this)])->firstOrFail();
    	$preference = $notifiable->notificationPreferences()->where(['notification_type_id' => $notificationType->id])->first();

    	if($preference) {
    		return $preference->via;
		}
        return $notificationType->default_via;
    }
    
    
}