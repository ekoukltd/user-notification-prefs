<?php

namespace Ekoukltd\UserNotificationPrefs\Datatables;

use Ekoukltd\UserNotificationPrefs\Helpers\Helper;
use Ekoukltd\UserNotificationPrefs\Models\NotificationType;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTableAbstract;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class NotificationTypesDatatables extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param  mixed  $query  Results from query() method.
     * @return DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('name',function($model){
                return $model->nameHeadline;
            })
        ->editColumn('recipient_type',function($model) {
        $res= "";
            foreach($model->recipient_type as $r){
                $res .= '<span class="badge rounded-pill me-1 badge-info bg-info"><i class="fa fa-user"></i> '.class_basename($r).'</span>';
            }
            return $res;
        })
        ->editColumn('default_via',function($model){
                $res = "";
                foreach($model->default_via as $via){
                    $res .= Helper::mapViaToBadge($via);
                }
                return $res;
                
            })
            ->rawColumns(['actions','default_via','recipient_type'])
			->addColumn('actions', 'ekoukltd::components.datatables.actions');
    }
    
    /**
     * Get query source of dataTable.
     *
     * @param  NotificationType  $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(NotificationType $model)
    {
        $query = $model->newQuery();
        
        
        return $query;
    }
    
    /**
     * Optional method if you want to use html builder.
     *
     * @return Builder
     */
    public function html()
    {
        $builder = $this->builder();
        $builder->columns($this->getColumns())
            ->orderBy(1, 'asc');
        $builder->setTableId("dt_".Str::snake(class_basename($this)));
        
        return $builder->minifiedAjax()
            ->responsive(false)
            ->info(true)
            ->dom(config('laravel-admin-tools.datatables.dom.'.config('laravel-admin-tools.css_format')))
            ->language([
                           'processing'        => config('laravel-admin-tools.datatables.processing'),
                           'search'            => "_INPUT_",
                           'searchPlaceholder' => "Search..",
                           'info'              => "<strong>_TOTAL_</strong> Notifications",
                           'paginate'          => [
                               'first'    => '<i class="fa fa-angle-double-left"></i>',
                               'previous' => '<i class="fa fa-angle-left"></i>',
                               'next'     => '<i class="fa fa-angle-right"></i>',
                               'last'     => '<i class="fa fa-angle-double-right"></i>'
                           ],
                       ]
            
            )
            ->buttons([])
            ->parameters([
                             'classes' => [
                                 'sWrapper'      => "dataTables_wrapper dt-".config('laravel-admin-tools.css_format'),
                                 'sFilterInput'  => "form-control form-control-lg",
                                 'sLengthSelect' => "form-control form-control-lg",
                             ],
                             'buttons' => []
            
                         ])
            ->pagingType('full_numbers')
            ->autoWidth(false)
            ->scrollX(true);
    }
    
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')
                ->hidden()
                ->className('d-none'),
            Column::make('name'),
            Column::make('recipient_type')->title('Recipients'),
            Column::make('default_via')->title('Via'),
            //Hiding this column as the table isn't wide enough to see everything
            //When we implement the parent module, will use full width tables to show as much as possible.
//            Column::make('email_template'),
//            Column::make('default_toarray')->title('Array'),
			Column::computed('actions')->className('text-center')->printable(false)
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return class_basename(self::class)."_".date('Y-m-d');
    }
}