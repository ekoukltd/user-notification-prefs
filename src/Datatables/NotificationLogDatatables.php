<?php
namespace Ekoukltd\UserNotificationPrefs\Datatables;


use Ekoukltd\UserNotificationPrefs\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class NotificationLogDatatables extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('user', function ($model) {
                return $model->notifiable;
            })
            ->filterColumn('user', function ($query, $keyword) {
                foreach (Notification::getAllUserTypes() as $model){
	                foreach($model->searchable as $att){
		                $query->orWhereRaw( $model->table.".$att like ?", ["%{$keyword}%"]);
	                }
                }
            })
            ->editColumn('type',function($model){
                return $model->nameHeadline;
                
            })
	        ->filterColumn('type', function ($query, $keyword) {
		        $query->whereRaw("notifications.type like ?", ["%{$keyword}%"]);
	        })
            ->editColumn('notifications.created_at',function($model){
                return $model->created_at?$model->created_at->format('d/m/Y H:i'):'';
            })

            ->rawColumns(['type','user','device','details'])
			->addColumn('details', 'ekoukltd::notification-meta.load-meta')
			->addColumn('device', 'ekoukltd::notification-meta.device');

    }
    
    /**
     * Get query source of dataTable.
     *
     * @param Notification $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Notification $model)
    {
        $query= $model->newQuery()->select('notifications.*');
    
        foreach (Notification::getAllUserTypes() as $model) {
            $query->leftJoin($model->table, function ($q) use ($model) {
                $q->on($model->table.'.id', '=', 'notifications.notifiable_id')
                  ->where('notifications.notifiable_type', $model->id);
            });
        }
    
        if ($this->request()->get('type')) {
            $query->where('notifications.type','like',"%".$this->request()->get('type')."%");
        }
        return $query;
    }
    
    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $builder = $this->builder();
        $builder->columns($this->getColumns())
                ->orderBy(2, 'desc');
        $builder->setTableId("dt_".Str::snake(class_basename($this)));
    
        return $builder->minifiedAjax()
                       ->responsive(false)
                       ->info(true)
                       ->dom(config('laravel-admin-tools.datatables.dom.'.config('laravel-admin-tools.css_format')))
                       ->language([
                                      'processing'        => config('laravel-admin-tools.datatables.processing'),
                                      'search'            => "_INPUT_",
                                      'searchPlaceholder' => "Search..",
                                      'info'              => "<strong>_TOTAL_</strong> Notifications",
                                      'paginate'          => [
                                          'first'    => '<i class="fa fa-angle-double-left"></i>',
                                          'previous' => '<i class="fa fa-angle-left"></i>',
                                          'next'     => '<i class="fa fa-angle-right"></i>',
                                          'last'     => '<i class="fa fa-angle-double-right"></i>'
                                      ],
                                  ]
    
                       )
                       ->buttons([])
                       ->parameters([
                                        'classes' => [
                                            'sWrapper'      => "dataTables_wrapper dt-".config('laravel-admin-tools.css_format'),
                                            'sFilterInput'  => "form-control form-control-lg",
                                            'sLengthSelect' => "form-control form-control-lg",
                                        ],
                                        'fnDrawCallback' => "function () {
                                            One.helpers(['bs-popover']);
                                           }",
                                        'buttons' => []
    
                                    ])
                       ->pagingType('full_numbers')
                       ->autoWidth(false)
                       ->scrollX(true);
    }
    
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
       return [
            Column::make('user'),
            Column::make('type')->title('Event'),
            Column::make('notifications.created_at')->title("When")->addClass('text-end'),
            Column::computed('device'),
            Column::computed('details')
            
        ];
    }
    
    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return class_basename(self::class)."_".date('Y-m-d');
    }
}