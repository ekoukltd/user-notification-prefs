<?php

return [
    'routes' => [
    	'admin' => [
    		'prefix' => 'notification-types',
			'middleware' => ['web','auth']
		],
        'logs' => [
            'prefix' => 'notification-logs',
            'middleware' => ['web','auth']
        ],
		'user' => [
			'prefix' => 'notifiable-preferences',
			'middleware' => ['web','auth']
		]
	],
	//User Models to apply preferences to
	'models' => [
        'App\Models\User',
        'App\Models\Admin',
	],
	'default_vias' => [
		'database',
		'mail',
		'sms',
	],
    //Prevent users from editing these vias. They will not be shown on the edit page
    //The database log creates our audit trail so no user should be able to disable logging.
    'disallow_via_preferences' => ['database'],
    
    //custom Notification Stub
    //If you publish the default stub it will be put in app/Stubs/notification.stub
	'stub' => 'app/Stubs/notification.stub'
];