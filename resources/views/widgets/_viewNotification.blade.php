<?php

use Ekoukltd\UserNotificationPrefs\Helpers\Highlight; ?>

<x-admintools-block title="Code Sample" blockclass="block-themed" headerclass="bg-info">
    <div class="sample">{!! $notificationType->usageExample !!}</div>
</x-admintools-block>

<x-admintools-block title="Generated Notification Class" blockclass="block-themed" headerclass="bg-amethyst-dark">
    <x-slot name="buttons">
        <a href="{{route(config('user-notification-prefs.routes.admin.prefix').'.edit',['notificationType'=>$notificationType])}}" class="ms-auto btn btn-primary">Edit Notification</a>
    </x-slot>


    <pre class="formatter">
   	 	<p style="margin-bottom: 0"><strong>File: {{$notificationType->fileName}}</strong></p>
    		{!! $notificationType->classContentsFormatted !!}
	</pre>
</x-admintools-block>

<style>
	.formatter, .sample {
		padding: 10px 20px;
		border: 1px solid #cccccc;
		background-color: #fff
	}

	.formatter table {
		display: inline-block;
	}

	.formatter table td:nth-child(1) {
		width: 40px;
	}

	tr.last-map {
		display: none;
	}

</style>