<?php
/**
 * @var $model \Ekoukltd\UserNotificationPrefs\Models\NotificationType
 */

use Ekoukltd\UserNotificationPrefs\Helpers\Helper;
use Illuminate\Support\Str;

$formAction = $model->exists ? 'update' : 'store';
$formMethod = $model->exists ? 'patch' : 'post';
$formTitle  = $model->exists ? 'Edit '.Str::headline($model->name) : 'Add New '.class_basename($model);
?>

<form method="post" action="{{route(config('user-notification-prefs.routes.admin.prefix').".$formAction",$model)}}" id="notificationTypeForm">
    @csrf
    @method($formMethod)
    <x-admintools-block title="{{$formTitle}}">

        @component('lat::components.'.config('laravel-admin-tools.css_format').'.inputs.text', [
                 'name' => 'name',
                 'id' => 'name',
                 'label' => 'Notification Class (will be converted to camelCase)',
                 'placeholder' => 'Notification Class',
                 'class'=>'js-camelify',
                 'value' => $model->name  ?? old('name'),
                 'required' => 'required'
            ])@endcomponent

        @component('lat::components.'.config('laravel-admin-tools.css_format').'.inputs.select',[
                          'name'=>'recipient_type[]',
                          'id'=>'recipient_type',
                          'label'=>'User models to apply to',
                          'multiple'=>true,
                          'class'=>'js-select2',
                          'selected'=>$model->recipient_type ?? old('recipient_type'),
                          'options'=>Helper::getUserModelSelect()
               ])@endcomponent

        @component('lat::components.'.config('laravel-admin-tools.css_format').'.inputs.select',[
                                      'name'=>'default_via[]',
                                      'id'=>'default_via',
                                      'label'=>'Send Notification Via (database, email, sms, push)',
                                      'multiple'=>true,
                                      'class'=>'js-select2',
                                      'selected'=>$model->default_via ?? old('default_via'),
                                      'options'=>Helper::getDefaultViaSelect()
                           ])@endcomponent

        @component('lat::components.'.config('laravel-admin-tools.css_format').'.inputs.select',[
                                                  'name'=>'properties[]',
                                                  'id'=>'properties',
                                                  'label'=>'Constructor models to inject (pick from discovered models or add a custom model)',
                                                  'multiple'=>true,
                                                  'tags'=>true,
                                                  'class'=>'js-select2',
                                                  'selected'=>$model->properties ?? old('properties'),
                                                  'options'=>Helper::getModelsAsProperties()
                                       ])@endcomponent

                @component('lat::components.'.config('laravel-admin-tools.css_format').'.inputs.toggle_inline',[
                                       'class'=>'js-preference-toggle',
                                       'checked'=>$model->hidden,
                                       'name'=>'hidden',
                                       'label'=>'Hide in user preferences',
                                       'value'=>1,
                                       'wrapperClass'=>'mb-4'
                                   ])@endcomponent

                @component('lat::components.'.config('laravel-admin-tools.css_format').'.inputs.toggle_inline',[
                       'class'=>'js-preference-toggle',
                       'checked'=>$model->readonly,
                       'name'=>'readonly',
                       'label'=>'Notification preference is read only for users',
                       'value'=>1,
                       'wrapperClass'=>'mb-0'
                   ])@endcomponent

    </x-admintools-block>

    <x-admintools-block title="<i class='fa fa-envelope'></i> Mail Options"
                        id="method-mail"
                        blockclass="{{!is_array($model->default_via) || !in_array('mail',$model->default_via)?'d-none':''}}">


        @component('lat::components.'.config('laravel-admin-tools.css_format').'.inputs.select',[
                                      'name'=>'email_template',
                                      'id'=>'email_template',
                                      'label'=>'Email Template (from \App\Mail Directory)',
                                      'class'=>'js-select2',
                                      'selected'=>$model->email_template ?? old('properties'),
                                      'options'=>Helper::getMailSelect()
                           ])@endcomponent


        <div class="requiresConstructor {{!is_array($model->properties)||!count($model->properties) ?'d-none':''}}">
            @component('lat::components.'.config('laravel-admin-tools.css_format').'.inputs.select',[
                                                       'name'=>'email_injector[]',
                                                       'id'=>'email_injector',
                                                       'label'=>'Models to inject into email template',
                                                       'multiple'=>true,
                                                       'tags'=>true,
                                                       'class'=>'js-select2',
                                                       'selected'=>$model->email_injector ?? old('email_injector'),
                                                       'options'=>Helper::getModelsAsProperties(),
                                                       'btnAfter'=>'Use models from constructor',
                                                       'btnAfterClass'=>'ms-auto btn-info btn-sm loadModelsFromConstructor'

                                            ])@endcomponent
        </div>
    </x-admintools-block>

    <x-admintools-block title="<i class='fa fa-database'></i> Database Options"
                        id="method-database"
                        blockclass="requiresConstructor {{!is_array($model->default_via)||!in_array('database',$model->default_via) || !count($model->properties) ?'d-none':''}}">

        @component('lat::components.'.config('laravel-admin-tools.css_format').'.inputs.textarea',[
                   'name' => 'default_toarray',
                   'id'=>'default_toarray',
                   'label'=>__('Array of models to save to db log'),
                   'class'=>'js-'.config('laraconsent.editor'),
                   'value' => trim("[\r\n".implode(",\r\n",($model->default_toarray??[]))."\r\n]") ?? old('default_toarray'),
                    'btnAfter'=>'Use models from constructor',
                   'btnAfterClass'=>'ms-auto btn-info btn-sm textAreaDefaults'

                   ])
        @endcomponent
    </x-admintools-block>

    <x-admintools-block title="<i class='fa fa-sms'></i> SMS Options"
                        id="method-sms"
                        blockclass=" {{!is_array($model->default_via)||!in_array('sms',$model->default_via) || !count($model->properties) ?'d-none':''}}">

        @component('lat::components.'.config('laravel-admin-tools.css_format').'.inputs.textarea',[
                              'name' => 'sms_content',
                              'id'=>'sms_content',
                              'label'=>__('Text message content'),
                              'class'=>'js-'.config('laraconsent.editor'),
                              'value' => trim($model->sms_content) ?? old('sms_content'),


                              ])
        @endcomponent
    </x-admintools-block>

    @isset ($errors)
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    @endisset


    <div class="d-grid gap-2">
        <div class="d-grid gap-2">
            <input type="submit" value="{{$model->exists?__('Update '.class_basename($model)):__('Save '.class_basename($model))}}" class="btn btn-lg btn-primary btn-block mb-4">
        </div>
    </div>
</form>
@push('scripts')
    <script>
        jQuery(function () {
            UserNotificationPrefs.helpers(['camelify', 'select2', 'initEditNotificationTypes']);
            UserNotificationPrefs.allVias = JSON.parse('{!! json_encode(config('user-notification-prefs.default_vias')) !!}');
        });
    </script>
@endpush