<x-admintools-block title="">
    @isset($addNewButton)
    <x-slot name="buttons">
        <a href="{{route('notification-types.create')}}" class="btn btn-primary" >Add New</a>
    </x-slot>
    @endisset
    @isset($notificationTypeDropDown)
        <x-slot name="buttons">
            <div class="d-flex justify-content-end align-items-center">
                <div class="me-2">Notification Filter</div>
                <div class="dt-filter-select" data-filter="type" href="javascript:void(0)">
                @component('lat::components.'.config('laravel-admin-tools.css_format').'.inputs.select',[
                                      'name'=>'type',
                                      'id'=>'notification_type',
                                      'style'=>"width:280px",
                                      'class'=>'js-select2',
                                      'selected'=>request()->get('type') ?? '',
                                      'options'=>\Ekoukltd\UserNotificationPrefs\Models\NotificationType::getNotificationTypeSelect(),
                                      'wrapperClass'=>''
                           ])@endcomponent
                </div>
            </div>


        </x-slot>
    @endisset

    {!! $dataTable->table(['class'=>'table '.($classes??'')],false)  !!}
</x-admintools-block>

@push('scripts')
    {!! $dataTable->scripts() !!}
@endpush
