<div class="col-md-6 col-xl-4">
    <div class="block block-rounded h-100 mb-0">
        <div class="block-header block-header-default">
            <h3 class="block-title">Notification Builder</h3>
        </div>
        <div class="block-content fs-sm text-muted">
            <p>Make managing notification classes easy and empower others to do the job.</p>

            <p>Notification Types are stored in the database and are used to dynamically build PHP notification classes.</p>

            <p>set requried vias including database, email, SMS or push and associate with email templates.</p>

            <p>Once created test the notifications directly in the system without writing any code.</p>

            <p>Speeds up and simplifies the process of adding notifications which may be used to create an audit log.</p>
            <div class="d-grid gap-2">
            <a class="btn btn-primary mb-2" href="{{route(config('user-notification-prefs.routes.admin.prefix').'.index')}}">View Notifications</a>
            </div>
        </div>
    </div>
</div>

<div class="col-md-6 col-xl-4">
    <div class="block block-rounded h-100 mb-0">
        <div class="block-header block-header-default">
            <h3 class="block-title">User Notification Preferences</h3>
        </div>
        <div class="block-content fs-sm text-muted">
            <p>Allow users to set their own notification preferences for Email, SMS and push messages.</p>

            <p>Just add the widget to a frontend page and users will be able to customise which notifications they want to recieve.</p>

            <p>Admins just have to set which user models should have the capability and add a trait to the user class.</p>

            <p>Instant update toggles via ajax with minimal configuration.</p>

            <div class="d-grid gap-2">
                <a class="btn btn-primary mb-2" href="{{route(config('user-notification-prefs.routes.user.prefix').'.index')}}">View Preferences</a>
            </div>
        </div>
    </div>
</div>

<div class="col-md-6 col-xl-4">
    <div class="block block-rounded h-100 mb-0">
        <div class="block-header block-header-default">
            <h3 class="block-title">Advanced Audit Log</h3>
        </div>
        <div class="block-content fs-sm text-muted">
            <p>Laravel events are perfect for creating an audit log of authentication events and any action that should be tracked.</p>

            <p>The advanced audit log uses Yarja datatables for super fast loading and filtering even when your logs have millions of rows.</p>

            <p>Quickly and easily create new notification events, simplifying compliance with IS27001 and provide visibilty of who did what and when. </p>

            <p>Save related models to the event which can be used to customise each log view to provide context relevant information.</p>

            <div class="d-grid gap-2">
                <a class="btn btn-primary mb-2" href="{{route(config('user-notification-prefs.routes.logs.prefix').'.index')}}">View Audit Log</a>
            </div>
        </div>
    </div>
</div>