
@php($adminPrefix = config('user-notification-prefs.routes.admin.prefix'))
@php($userPrefix = config('user-notification-prefs.routes.user.prefix'))
@php($logsPrefix = config('user-notification-prefs.routes.logs.prefix'))

<li class="nav-main-item{{ request()->is(["$adminPrefix*"]) ? ' open' : '' }}">
    <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
        <i class="nav-main-link-icon fa fa-bell"></i>
        <span class="nav-main-link-name">Notification Builder</span>
    </a>
    <ul class="nav-main-submenu">
        <li class="nav-main-item">
            <a class="nav-main-link{{ Route::is($adminPrefix.'.index') ? ' active' : '' }}" href="{{route( $adminPrefix.".index")}}">
                <i class="nav-main-link-icon si si-grid"></i>
                <span class="nav-main-link-name">All Notifications</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link{{ Route::is($adminPrefix.'.create') ? ' active' : '' }}" href="{{route( $adminPrefix.".create")}}">
                <i class="nav-main-link-icon fa fa-plus-circle"></i>
                <span class="nav-main-link-name">Add new</span>
            </a>
        </li>


    </ul>
</li>

<li class="nav-main-item">
    <a class="nav-main-link{{ Route::is($userPrefix.'.index') ? ' active' : '' }}" href="{{route( $userPrefix.'.index')}}">
        <i class="nav-main-link-icon fa fa-check-circle"></i>
        <span class="nav-main-link-name">My Preferences</span>
    </a>
</li>

<li class="nav-main-item">
    <a class="nav-main-link{{ request()->is(["$logsPrefix*"]) ? ' active' : '' }}" href="{{route( $logsPrefix.'.index')}}">
        <i class="nav-main-link-icon  fa fa-database"></i>
        <span class="nav-main-link-name">Audit Logs</span>
    </a>
</li>
