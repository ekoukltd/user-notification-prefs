@extends('lat::layouts.backend')
@section('pageHeader')
    <x-admintools-page-title icon="plus-circle" :title="__('Add New Notification')" subtitle="Builds a new Notification Class" />
@endsection

@section('metaTitle',__('Add New Notification'))

@section('content')
    @include('vendor.ekoukltd.user-notification-prefs.widgets._createEditForm')
@endsection

