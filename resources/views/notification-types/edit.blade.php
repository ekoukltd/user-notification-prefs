@extends('lat::layouts.backend')

@section('pageHeader')
    <x-admintools-page-title icon="pen-nib" :title="__('Edit Notifications')" subtitle="Update the notification class" />
@endsection

@section('metaTitle',__('Edit Notification'))

@section('content')
    @include('vendor.ekoukltd.user-notification-prefs.widgets._createEditForm')
@endsection

