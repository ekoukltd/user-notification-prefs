@extends('lat::layouts.backend')

@section('pageHeader')
    <x-admintools-page-title icon="bell" :title="__('Notification Types')" subtitle="View and test notifications" />
@endsection
@section('metaTitle',__('Notification Types'))

@section('content')
     @include('vendor.ekoukltd.user-notification-prefs.widgets._dataTable',['addNewButton'=>true])

@endsection


@push('scripts')
    <script>
        jQuery(function () {
            LaraAdminTools.helpers('initAjaxLoadModal');
        });
    </script>
@endpush



