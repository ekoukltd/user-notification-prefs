@extends('lat::layouts.backend')

@section('pageHeader')
    <x-admintools-page-title icon="code" :title="__('View Code')" :subtitle="\Illuminate\Support\Str::headline($notificationType->name)"/>
@endsection

@section('metaTitle',Illuminate\Support\Str::headline($notificationType->name))

@section('content')
    @include('vendor.ekoukltd.user-notification-prefs.widgets._viewNotification')
@endsection
