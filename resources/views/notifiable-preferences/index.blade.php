@extends('lat::layouts.backend')

@section('pageHeader')
<x-admintools-page-title icon="check-circle" :title="__('Notification Preferences')" subtitle="Set your alert channel optionss" />
@endsection
@section('metaTitle',__('User Preferences'))

@section('content')
    <x-admintools-block title="Notification Preferences">

        <table class="table table-vcenter table-striped table-hover ">
            <thead>
            <tr>
                <th>
                    Notification Name
                </th>
                @foreach($availableVias as $via)
                    <th>
                        {{$via}}
                    </th>
                @endforeach
            </tr>
            </thead>

            @foreach($notificationTypes as $notificationType)
                @if(!$notificationType->hidden)
                <tr>
                    <td>{{$notificationType->nameHeadline}}</td>
                    @foreach($availableVias as $via)
                        <td>
                            @if(in_array($via,$notificationType->default_via))
                                <form action="{{route(config('user-notification-prefs.routes.user.prefix').'.update')}}" method="POST">
                                    @csrf
                                    @component('lat::components.bootstrap5.inputs.toggle_inline',[
                                        'class'=>'js-preference-toggle',
                                        'checked'=>in_array($via, json_decode($notificationType->preference_via)),
                                        'name'=>'checked',
                                        'readonly'=>$notificationType->readonly,
                                        'unavailable'=>!in_array($via, $notificationType->default_via) ? '1' : '0',
                                        'value'=>1,
                                        'wrapperClass'=>'mb-0'
                                    ])@endcomponent
                                    <input type="hidden" name="via" value="{{$via}}"/>
                                    <input type="hidden" name="id" value="{{$notificationType->preference_id}}"/>
                                    <input type="hidden" name="notification_type_id" value="{{$notificationType->id}}"/>
                                </form>
                            @endif
                        </td>
                    @endforeach
                @endif
            @endforeach
        </table>
    </x-admintools-block>
    @include('vendor.ekoukltd.laravel-admin-tools.components.modals.empty')
@endsection

@push('scripts')
    <script>
        jQuery(function () {
            UserNotificationPrefs.helpers(['updateNotificationPreference']);
        });
    </script>
@endpush

