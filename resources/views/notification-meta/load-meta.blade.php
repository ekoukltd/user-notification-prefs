@php
    /**
    * @var $model Ekoukltd\UserNotificationPrefs\Models\Notification;
    */
    $view = "vendor.ekoukltd.user-notification-prefs.notification-meta." . class_basename($model->type);

@endphp
<div class="d-flex align-items-center">
@includeIf($view)
</div>

