@php
    /**
    * @var $model Ekoukltd\UserNotificationPrefs\Models\Notification;
    */

@endphp
@if($model->agentDataString)
    <div class="d-flex align-items-center">
        <button type="button" class="btn btn-sm btn-warning me-2 js-bs-popover"
                data-bs-toggle="popover"
                data-bs-animation="true"
                data-bs-placement="top" title=""
                data-bs-html="true"
                data-bs-content="{{$model->agentDataString}}"
                data-bs-original-title="Device Info">
            <i class="fa fa-fw fa-{{$model->deviceIcon}}"></i>
        </button>
        {{$model->city}}
    </div>
@endif


