@extends('lat::layouts.backend')

@section('pageHeader')
    <x-admintools-page-title icon="database" :title="__('Notification Logs')" subtitle="View the audit log" />
@endsection

@section('metaTitle',__('Notification Logs'))

@section('content')
     @include('vendor.ekoukltd.user-notification-prefs.widgets._dataTable',['notificationTypeDropDown'=>true])
@endsection

@push('scripts')
    <script>
        jQuery(function () {
            LaraAdminTools.helpers([ 'select2','initDatatablesFilters']);
        });
    </script>
@endpush


