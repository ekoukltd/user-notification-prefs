<?php
/**
 * Could be any model, just added to remove syntax error in Phpstorm.  Is only a comment
 * @var $model \App\Models\User;
 */

?>
<div class="btn-group mx-1" role="group" aria-label="Icons File group">
        <a type="button"
                class="btn btn-sm btn-info"
                href="{{route('notification-types.show',['notificationType'=>$model])}}"
                title="Delete"><i class="fa fa-eye "></i></a>
        <a type="button"
                class="btn btn-sm btn-success"
                href="{{route('notification-types.edit',['notificationType'=>$model])}}"
                title="Edit"><i class="fa fa-edit"></i></a>
        <button type="button"
                class="btn btn-sm btn-warning"
                data-bs-toggle="modal"
                data-bs-target="#emptyModal"
                data-href="{{route('unp.modal.confirm.emit',['notificationType'=>$model->id])}}"
                title="Send Test Notification"><i class="fa fa-broadcast-tower"></i></button>
        <button type="button"
                class="btn btn-sm btn-danger"
                data-bs-toggle="modal"
                data-bs-target="#emptyModal"
                data-href="{{route('unp.modal.confirm.delete',['id'=>$model->id])}}"
                title="Show"><i class="fa fa-trash"></i></button>
</div>

