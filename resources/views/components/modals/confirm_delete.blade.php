<x-admintools-modal-content title='<i class="icon fa fa-question-circle" aria-hidden="true"></i> Delete Record'>
    <x-slot name="footerButtons">
        <form action="{{route(config('user-notification-prefs.routes.admin.prefix').'.destroy',['notificationType'=>$model])}}" method="POST">
            @method('delete')
            @csrf
            <button type="button" class="btn btn-lg btn-alt-warning ms-auto mr-2" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-lg btn-danger">Delete</button>
        </form>
    </x-slot>
    <div class="block-content-full text-center">
        <p>Are you sure you want to delete the notification {{$model->nameHeadline}}?</p>

        <p><b>Note:</b> This action can not be undone.</p>
        <p></p>
    </div>
</x-admintools-modal-content>
