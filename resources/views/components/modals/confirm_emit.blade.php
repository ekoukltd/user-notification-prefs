<x-admintools-modal-content title='<i class="icon fa fa-broadcast-tower" aria-hidden="true"></i> Emit Notification'>
    <x-slot name="footerButtons">
        <form action="{{route(config('user-notification-prefs.routes.admin.prefix').'.emit',['notificationType'=>$model])}}" method="POST">
            @method('post')
            @csrf
            <button type="button" class="btn btn-lg btn-alt-warning mr-2" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-lg btn-danger">Trigger Send Notification</button>
        </form>
    </x-slot>
    <div class="block-content-full text-center">
        <p>Are you sure you want to send the {{$model->nameHeadline}} notification?</p>

        <p></p>
    </div>
</x-admintools-modal-content>
