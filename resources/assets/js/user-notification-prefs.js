import Helpers from './helpers';

export default class UserNotificationPrefs {

    constructor() {
        this._Init();
    }

    allVias = [];

    _Init() {
        this.helpers([
            //add helpers here that should be autoloaded on every page

        ]);
        console.log('User Notification Preferences Initialised');
    }


    init() {
        this._Init();
    }

    /*
     * jQuery Class Helpers
     *
     */
    helpers(helpers, options = {}) {
        Helpers.run(helpers, options);
    }

}

// Once everything is loaded
jQuery(() => {
    // Create a new instance of LaraConsent
    window.UserNotificationPrefs = new UserNotificationPrefs();
});
