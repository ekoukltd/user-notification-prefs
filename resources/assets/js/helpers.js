// Helpers
export default class Helpers {

   static run(helpers, options = {}) {
       let allHelpers = {
           select2: () => this.select2(),
           slugify: () => this.slugify(),
           camelify: () => this.camelify(),

           updateNotificationPreference: () => this.updateNotificationPreference(),
           initEditNotificationTypes: () => this.initEditNotificationTypes()
       };

       if (helpers instanceof Array) {
           for (let index in helpers) {
               if (allHelpers[helpers[index]]) {
                   allHelpers[helpers[index]](options);
               }
           }
       } else {
           if (allHelpers[helpers]) {
               allHelpers[helpers](options);
           }
       }
   }

    /*
     * Select2, for more examples you can check out https://github.com/select2/select2
     */
    static select2() {
        jQuery('.js-select2:not(.js-select2-enabled)').each((index, element) => {
            let el = jQuery(element);
            el.addClass('js-select2-enabled').select2({
                placeholder: el.data('placeholder') || false,
                tags: (el.data('tags')?true:false) || false,

            });
        });
    }

    static slugify()
    {
        String.prototype.slugify = function (separator = "-") {
            return this
                .toString()
                .normalize('NFD')                   // split an accented letter in the base letter and the acent
                .replace(/[\u0300-\u036f]/g, '')   // remove all previously split accents
                .toLowerCase()
                .replace(/\s+/g, '-')
                .replace(/-+/g, '-');

        };
    }

    static camelify()
    {
        String.prototype.camelify = function () {
            return this
                .replace(/(?:^\w|[A-Z]|\b\w)/g, function (word, index) {
                    return index === 0 ? word.toLowerCase() : word.toUpperCase();
                })
                .replace(/\s+/g, '');
        }

        jQuery('.js-camelify:not(.js-camelify-enabled)').each((index, element) => {
            let el = jQuery(element);
            el.addClass('js-camelify-enabled');
            jQuery(element).on('blur paste',function(){
                jQuery(this).val(jQuery(this).val().camelify());
            });
        });
    }

    static updateNotificationPreference()
    {
        jQuery(document).on('change','.js-preference-toggle', function (e) {
            let input = jQuery(this);
            let form = input.closest('form');

            jQuery.ajax({
                url: form.action,
                method: "POST",
                dataType: 'json',
                data: form.serializeArray(),
                success: function (data) {
                    if (data.hasOwnProperty('success')) {
                        window.LaraAdminTools.helpers('notify', {type: data.colour, icon: 'fa fa-check mr-1', message: data.message});
                    }
                    if (data.hasOwnProperty('error')) {
                        //Undo Toggle
                        input.prop("checked", !input.prop("checked"));
                        window.LaraAdminTools.helpers('notify', {type: 'danger', icon: 'fa fa-times mr-1', message: data.message});
                    }

                },
                error: function () {
                    input.prop("checked", !input.prop("checked"));
                    window.LaraAdminTools.helpers('notify', {type: 'danger', icon: 'fa fa-times mr-1', message: "There was an error updating your preference"});
                }
            });
        });
    }


    //Handles showing and hiding of relevant sections on Edit Notifications and populating injectors from constructor
    static initEditNotificationTypes()
    {
        jQuery(document).on('click','.loadModelsFromConstructor', function (e) {
            let dest = jQuery(this).closest('div').find('select');
            let options = jQuery("#properties").select2('data');
            let v;
            dest.empty();
            for (v of Object.values(options)) {
                let newOption = new Option(v.text, v.id, true, true);
                dest.append(newOption).trigger('change');
            }
        });

        jQuery(document).on('click','.textAreaDefaults', function (e) {
            let dest = jQuery(this).closest('div').find('textarea');
            let options = jQuery("#properties").select2('data');
            let content = "[";
            let v;
            for (v of Object.values(options)) {
                content +="\r\n\t'"+v.id+"' => $this->"+v.id+"->id,";
            }
            content = content.slice(0, -1);
            content +="\r\n]";
            dest.val(content);
        });

        jQuery(document).on('change','#properties',function(e) {
            let options = jQuery(this).select2('data');
            if(options.length){
               //Constructor options should allow to_array and email injectors to be visible.
                jQuery('.requiresConstructor').removeClass('d-none');
            } else {
                //Remove injectors from emails and toArray
                jQuery('#email_injector').empty().trigger('change');
                jQuery('#default_toarray').val("[\r\n\t  \r\n]");
                //Hide the inputs
                jQuery('.requiresConstructor').addClass('d-none');
            }

        });

        jQuery(document).on('change','#default_via',function(e) {
            let options = jQuery(this).select2('data');
            if(options.length){
                UserNotificationPrefs.allVias.forEach(element=>{
                    jQuery('#method-'+element).addClass('d-none');
                });
                let v;
                for (v of Object.values(options)) {
                    jQuery('#method-'+v.id).removeClass('d-none');
                }
            } else {
                UserNotificationPrefs.allVias.forEach(element=>{
                    jQuery('#method-'+element).addClass('d-none');
                });
            }

        });



    }

}
