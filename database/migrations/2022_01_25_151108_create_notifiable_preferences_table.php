<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotifiablePreferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifiable_preferences', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('notification_type_id');
            $table->string('notifiable_type');
            $table->integer('notifiable_id');
            $table->json('via');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifiable_preferences');
    }
}
