<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->json('recipient_type');
            $table->json('default_via')->nullable();
            $table->json('properties')->nullable();
            $table->string('email_template')->nullable();
            $table->text('sms_content')->nullable();
            $table->json('email_injector')->nullable();
            $table->json('default_toarray')->nullable();
            $table->boolean('readonly')->default(false);
            $table->boolean('hidden')->default(false);
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_types');
    }
}
