<?php

namespace Ekoukltd\UserNotificationPrefs\Database\Seeders;

use Ekoukltd\UserNotificationPrefs\Models\NotificationType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NotificationTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* Map Rows and Loop Through Them */
        $rows   = array_map('str_getcsv', file(dirname(__FILE__).'/gw_notifications.csv'));
        $header = array_shift($rows);
        $jsonColumns = ['recipient_type','default_via','email_injector','default_toarray','properties'];
        $csv    = [];
        foreach($rows as $row) {
            echo print_r($row,true)."count ".count($row);
            $arrayRow = array_combine($header, $row);
            $arrayRow['created_at']=$arrayRow['updated_at']=now();
            foreach($jsonColumns as $col){
                //Encode Json Columns data
                $arrayRow[$col]= (strlen($arrayRow[$col])?json_encode(str_getcsv($arrayRow[$col])):json_encode([]));
            }
            $csv[] = $arrayRow;
        }
        //Requires Laravel 8  -> Bulk method to insert or update
        NotificationType::upsert($csv,['name'],array_keys($csv[0]));
        
    }
}
