# Dynamic Notification Builder with User Preferences

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Total Downloads][ico-downloads]][link-downloads]
[![Build Status][ico-travis]][link-travis]
[![StyleCI][ico-styleci]][link-styleci]

The Laravel notification system is amazingly simple and just works, this module expands on these capabilities to provide:-

- User Notification Preferences allowing users to choose which notifications they should recieve from available channels.
- Dynamic notifications createable and editable in the database.  Notification classes are built by the system.
- Notification log viewer
- Notification tester - send test notifications to more easily test content



## Installation

Via Composer

``` bash
$ composer require ekoukltd/user-notification-prefs
```

If the laravel notifications table has not already been installed run:-
``` bash
php artisan notifications:table
php artisan migrate
```

Publish the config file:-
```bash
php artisan vendor:publish --tag=user-notification-prefs.config
```

Add the ``HasNotificationPreferences`` trait to any user models which should be able to customise their notification preferences.
These models should already have the Notifiable Trait included.

eg: A user class should include these traits:-
```injectablephp

namespace App\Models;

use Ekoukltd\UserNotificationPrefs\Traits\HasNotificationPreferences;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasNotificationPreferences;
```

To provide a more enhanced Notification / Audit log the [stevebauman/location](https://github.com/stevebauman/location) module is included to lookup IP addresses to locations.
This module uses the [ipApi](http://ip-api.com/) service by default, but may also be switched to other providers, we automate downloading the maxmind database on a weekly basis.



## Usage

## Change log

Please see the [changelog](changelog.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [contributing.md](contributing.md) for details and a todolist.

## Security

If you discover any security related issues, please email author@email.com instead of using the issue tracker.

## Credits

- [Author Name][link-author]
- [All Contributors][link-contributors]

## License

MIT. Please see the [license file](license.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/ekoukltd/user-notification-prefs.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/ekoukltd/user-notification-prefs.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/ekoukltd/user-notification-prefs/master.svg?style=flat-square
[ico-styleci]: https://styleci.io/repos/12345678/shield

[link-packagist]: https://packagist.org/packages/ekoukltd/user-notification-prefs
[link-downloads]: https://packagist.org/packages/ekoukltd/user-notification-prefs
[link-travis]: https://travis-ci.org/ekoukltd/user-notification-prefs
[link-styleci]: https://styleci.io/repos/12345678
[link-author]: https://github.com/ekoukltd
[link-contributors]: ../../contributors
